#!/usr/bin/python3

import json
import os
import time

for folder in ['public', 'public/icons']:
  if not os.path.exists(folder):
      os.makedirs(folder)

# Generate test html
with open('public/index.html', 'w+') as file:
  file.write("<html><body>бифот съел деда</body></html>")


# Fetch resources
with open('resources.json') as json_file:
  resources_root = json.load(json_file)
  
  for i in range(len(resources_root)):
    os.system('wget -q -O public/'+resources_root[i]["as"]+' '+resources_root[i]["from"])
    time.sleep(1) # Don't wget too fast


# Generate index_v1
index_v1 = {}

with open('head.json') as json_file:
  index_v1.update(json.load(json_file))


with open('apps.json') as json_file:
  apps_root = json.load(json_file)

  # Create packages section
  resulting_root = {}
  packages = {}

  for i in range(len(apps_root)):
    app_meta = apps_root[i]
    app_meta["lastUpdated"] = round(time.time() * 1000)

    meta_base_url = app_meta.pop("metaBaseUrl", None)
    app_meta["sourceCode"] = meta_base_url
    app_meta["changelog"] = meta_base_url + "/-/tags"
    app_meta["issueTracker"] = meta_base_url + "/-/issues"

    # Generate package entry
    pkg_dict = {
      "packageName": app_meta["packageName"],
      "added": app_meta["lastUpdated"],
      "versionCode": app_meta["suggestedVersionCode"],
      "versionName": app_meta["suggestedVersionName"],
      "added": app_meta["lastUpdated"],
      "hashType": "sha256"
    }

    # Move special keys from app entry to package
    for key in ["minSdkVersion", "targetSdkVersion", "uses-permission"]:
      pkg_dict[key] = app_meta.pop(key, None)

    apk_name = app_meta.pop("apkName", None)
    apk_pubname = 'public/'+apk_name
    apk_url = app_meta.pop("apkUrl", None)
    sha_hash = ""
    signer = ""

    # Download apk
    os.system('wget -q -O '+apk_pubname+' '+apk_url)

    # Calculate hash
    os.system('sha256sum '+apk_pubname+' > hashsum.txt')
    with open('hashsum.txt') as hashfile:
      sha_hash = hashfile.read().replace(apk_pubname, '').strip()

    # Get signer
    siggrep = "Signer #1 certificate SHA-256 digest: "
    os.system('apksigner verify --print-certs public/git-notes.apk | grep \''+siggrep+'\' > sig.txt')
    with open('sig.txt') as sigfile:
      signer = sigfile.read().replace(siggrep, '').strip()

    pkg_dict["apkName"] = apk_name
    pkg_dict["hash"] = sha_hash
    pkg_dict["sig"] = app_meta.pop("signatureMd5", None)
    pkg_dict["signer"] = signer
    pkg_dict["size"] = os.path.getsize(apk_pubname)

    packages[ app_meta["packageName"] ] = [ pkg_dict, ]

  resulting_root["apps"] = apps_root
  resulting_root["packages"] = packages
  index_v1.update(resulting_root)


# Write index_v1
with open('public/index-v1.json', 'w') as outfile:
  outfile.write(json.dumps(index_v1, ensure_ascii=False))
